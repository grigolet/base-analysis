import pandas as pd
from pathlib import Path
from numba import jit
import numpy as np
import termplotlib as tpl

class BaseAnalysis:
    """Class that analyses the content of a single
    hv folder, storing the efficiency and the 
    streamer probability in its attributes"""
    def __init__(self, hv_folder=None, adc_to_mv=None, sampling_time=2e-9,
                 resistance=50, height_threshold=0.5,
                 baseline_offset=None, signal_interval=None, 
                 record_length=None,
                 channel_mapping=None,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hv_folder = Path(hv_folder)
        self.adc_to_mv = adc_to_mv
        self.height_threshold = height_threshold
        self.baseline_offset = baseline_offset or 0.2
        self.record_length = record_length or 520
        self.sampling_time = sampling_time
        self.resistance = resistance
        self.signal_interval = signal_interval or [0.5, 1]
        self.channel_mapping = channel_mapping or {
            'RPC4': [1, 2, 3, 4, 5, 6, 7, 8],
            'RPC5': [9, 10, 11, 12, 13, 14, 15],
        }


        self.charge_factor = self.adc_to_mv * self.sampling_time / self.resistance

        self._df = None

        self.efficiency = {}
        self.efficieny_error = {}
        self.streamer_probability = {}
        self.streamer_probability_error = {}

        if any(interval <= 1 for interval in self.signal_interval):
            self.min_signal_time_sample = self.calc_time_sample(self.signal_interval[0], self.record_length)
            self.max_signal_time_sample = self.calc_time_sample(self.signal_interval[1], self.record_length)
        else:
             self.min_signal_time_sample = self.signal_interval[0]   
             self.max_signal_time_sample = self.signal_interval[1]   

    def calc_time_sample(self, percent, maximum):
        return int(percent * maximum)


    def read_raw_data(self, wavefile):
        df = pd.read_csv(wavefile, header=None)
        values = df.values.reshape((-1, self.record_length))
        return values


    @staticmethod
    def plot_event(data):
        fig = tpl.figure()
        y = np.arange(data.shape[0])
        fig.plot(y, data, width=100, height=30)
        fig.show()


    def create_df(self):
        self._df = pd.DataFrame()
        for wavefile in self.hv_folder.glob('wave*.txt'):
            channel = int(wavefile.stem.strip('wave'))
            data = self.read_raw_data(wavefile)
            self.events_number = data.shape[0]
            ix_baseline = int(self.baseline_offset * self.record_length)
            baselines = data[:, :ix_baseline].mean(axis=1)
            height_data = (- data + baselines[:, np.newaxis]) * self.adc_to_mv
            max_heights = height_data.max(axis=1)
            crossing_times = np.where(height_data > self.height_threshold, np.arange(self.record_length), self.record_length + 1).min(axis=1)
            # import pdb; pdb.set_trace()
            df = pd.DataFrame({
                'channel': channel,
                'baselines': baselines,
                'max_heights': max_heights,
                'crossing_times': crossing_times,
                'event': np.arange(baselines.shape[0])
            })
            self._df = self._df.append(df)


    def channel_to_rpc(self, channel):
        for rpc, channels in self.channel_mapping.items():
            if channel in channels:
                return rpc


    def map_channels_to_rpc(self):
        self._df['rpc'] = self._df['channel'].apply(self.channel_to_rpc).astype('category')

    
    def detect_events(self):
        self._df['detected'] = (self._df.crossing_times < self.max_signal_time_sample) & (self._df.crossing_times > self.min_signal_time_sample)
        self._df['efficient_event'] = self._df.groupby(['rpc', 'event']).detected.transform(np.any)


    def calc_efficiency(self):
        for rpc, group in self._df.groupby('rpc'):
            self.efficiency[rpc] = group.efficient_event.sum() / group.efficient_event.size


    def run(self):
        self.create_df()
        self.map_channels_to_rpc()
        self.detect_events()
        self.calc_efficiency()


