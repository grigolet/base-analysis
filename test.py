from BaseAnalysis import BaseAnalysis

hv_folder = '/home/gianluca/Documents/CERN/data/data-test-beam-aug-2018/20180810/no-header-512/Abs232/MIX0/HV8'
adc_to_mv = 0.122
channel_mapping = {
    'RPC0': [1,2,3,4,5,6,7],
    'RPC1': [8,9,10,11,12,13,14,15]
}

analysis = BaseAnalysis(hv_folder=hv_folder, adc_to_mv=adc_to_mv, baseline_offset=0.3,
                        signal_interval=[0.4, 0.8], record_length=520, channel_mapping=channel_mapping)


analysis.run()
print(analysis.efficiency)